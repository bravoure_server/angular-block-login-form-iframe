(function () {
    'use strict';

    function blockLoginFormIframe (PATH_CONFIG, $rootScope) {
        return {
            restrict: "E",
            replace: "true",
            link: function (scope, element, attrs) {
            },
            controller: function ($scope) {
                $scope.login_url = host + 'login?lang=' + PATH_CONFIG.current_language + '&redirect_uri=' + host + 'authorize';

                $scope.showIframe = ($scope.$state.params.identifier == 'top.login_page') ? true : false;

                $rootScope.$on('hideIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = false;
                    });
                });

                $rootScope.$on('showIframe', function () {
                    $scope.$apply(function () {
                        $scope.showIframe = true;
                    });
                });

            },
            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-login-form-iframe/block-login-form-iframe.html'
        }
    }

    blockLoginFormIframe.$inject = ['PATH_CONFIG', '$rootScope'];

    angular
        .module('bravoureAngularApp')
        .directive('blockLoginFormIframe', blockLoginFormIframe);
})();
